"use strict";

import fs from "fs";
import { Octokit } from "@octokit/rest";
import NodeCache from "node-cache";
let cachedValue = new NodeCache({ stdTTL: 100, checkperiod: 120 });

import ErrorResponse from "../utils/errorResponse.js";

export const getHomeDirectory = async (req, res, next) => {
  res.send("This is the root directory : )");
};

export const getRepoStars = async (req, res, next) => {
  //Destructure request parameters
  const { owner, repo_name } = req.params;

  let owners = owner.split("&");
  let repos = repo_name.split("&");

  if (owners.length !== repos.length) {
    return next(
      new ErrorResponse(
        "Bad request, owners and repo names length must be the same.",
        400
      )
    );
  }

  try {
    const data = await APIFeature(owners, repos);
    res.status(200).json(data);
  } catch (error) {
    return new ErrorResponse(error.message, error.status);
  }
};

export const searchString = async (req, res, next) => {
  const searchString = [];
  let str;
  try {
    const data = cachedValue.get("dataCollection");
    if (data == null) {
      let stream = fs.createReadStream("./data/industry_sic.csv");

      stream.on("data", (data) => {
        let chunk = data.toString();
        str = chunk.split(",");
      });
      stream.on("end", () => {
        cachedValue.set("dataCollection", str, 10000);
        str.filter((item) => {
          if (item.toLowerCase().includes(req.params.searchString.toLowerCase())){
            searchString.push(item);
          }
        });
        return res.json({
          ValueFrom: "Value from new request",
          searchString,
        });
      });
    } else {
      let dataValue = data.filter((item) => {
        return item.toLowerCase().includes(req.params.searchString.toLowerCase())
      });    
      return res.json({
        "ValueFrom": "Cached value from node-cache",
         "searchString" : dataValue
      });
    }
  } catch (error) {
    return next(new ErrorResponse(error.message, error.status));
  }
};

const APIFeature = async (owners, repos) => {
  const octokit = new Octokit({
    auth: process.env.AUTH,
  });
  try {
    let dataCollection = [];
    const ownersSize = owners.length
    for (let i = 0; i < ownersSize; i++) {
      const response = await octokit.request("GET /repos/{owner}/{repo}", {
        owner: owners[i],
        repo: repos[i],
      });

      dataCollection = [
        ...dataCollection,
        {
          "Repo Name": response.data.name,
          "Repo Description": response.data.description,
          "Number of Star": response.data.stargazers_count,
        },
      ];
    }
    return dataCollection;
  } catch (error) {
    return new ErrorResponse(error.message, error.status);
  }
};
